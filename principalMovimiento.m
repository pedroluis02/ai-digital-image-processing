function principalMovimiento(imagen_1, imagen_2)
   img_1 = imread(imagen_1);
   img_2 = imread(imagen_2);
   
   gray_1 = escalaGrises(img_1, 1);
   gray_2 = escalaGrises(img_2, 1); 
   
   figure(5);
   subplot(2, 2, 1); 
   imshow(gray_1); title('Imagen 1');
   subplot(2, 2, 2);  
   imshow(gray_2); title('Imagen 2');
   
   matrizBordes_1 = obtenerMatrizBorde(gray_1, 14, 14);
   matrizBordes_2 = obtenerMatrizBorde(gray_2, 14, 14);
   
   imagen_seg_1 = segmentarImagen(gray_1, matrizBordes_1);
   imagen_seg_2 = segmentarImagen(gray_1, matrizBordes_2);
     
   subplot(2, 2, 3); 
   imshow(imagen_seg_1); title('Imagen 1');
   subplot(2, 2, 4);  
   imshow(imagen_seg_2); title('Imagen 2');
   
   cuadrantes_1 = cuadrantesImagen(gray_1, 3);	
   cuadrantes_2 = cuadrantesImagen(gray_2, 3);
   
   [fc, cc] = size(cuadrantes_1);
   i = 1; 
   c1 = []; c2 = [];
   h1 = []; h2 = [];
   l1 = zeros(fc, 1); l2 = zeros(fc, 1);
   while i <= fc
      c1 = cuadrantes_1(i, :);
      h1 = histogramaGray(imagen_seg_1, c1);
      l1(i) = h1(1);

      c2 = cuadrantes_2(i, :);
      h2 = histogramaGray(imagen_seg_2, c2);
      l2(i) = h2(1);      

      i = i + 1;
   end 
   b = 0;
   disp('Movimiento: ');
   
   if l1(4) < l2(4)
      disp('Izquierda');
      b = 1;
   end
   
   if l1(6) < l2(6)
      disp('Derecha');
      b = 1;
   end
   
   if l1(3) < l2(3)
      disp('Arriba');
      b = 1;
   end	
   
   if l1(8) < l2(8)
      disp('Abajo');
      b = 1;
   end	
   
   if b == 0
      disp('Ninguno');
   end
   
end