% Funcion: recibe una imagen en escala de grises y retorna una matriz de bordes
  % imagen: imagen en escala de grises
  % nIzquierda: parametro de incio valor izquierdo de borde
  % nDerecha: parametro de incio valor derecho de borde
% matrizBordes: matriz de bordes encontrados
function matrizBordes = obtenerMatrizBorde(imagen, nIzquierda, nDerecha)
   [filas, columnas] = size(imagen);
   mitad_columnas = round(columnas / 2);
   matrizBordes = zeros(filas, 2);
   % ---- iteradores
   k_izquierda = 1; k_derecha = columnas;
   % ---- variables para comparacion
   punto_Izquierda = -1; punto_Derecha = -1;
   resta = 0; gris_1 = 0; gris_2 = 0;
   % ----
   
   for i = 1: filas
      matrizBordes(i, 1) = -1; 
      matrizBordes(i, 2) = -1;
      while k_izquierda <= mitad_columnas
         gris_1 = imagen(i, k_izquierda);
         gris_2 = imagen(i, k_izquierda + 1);
         resta = abs(gris_1 - gris_2);
         
         if resta >= nIzquierda && punto_Izquierda == -1
            punto_Izquierda = k_izquierda + 1;
         	  matrizBordes(i, 1) = punto_Izquierda;
         end
         
         gris_1 = imagen(i, k_derecha);
         gris_2 = imagen(i, k_derecha - 1);
         resta = abs(gris_1 - gris_2);
         
         if resta >= nDerecha && punto_Derecha == -1
         	  punto_Derecha = k_derecha - 1;
         	  matrizBordes(i, 2) = punto_Derecha;
         end
         
         k_izquierda = k_izquierda + 1;
         k_derecha = k_derecha - 1;
      end
      
      punto_Izquierda = -1; 
      punto_Derecha = -1;
      % ---
      k_izquierda = 1;
      k_derecha = columnas;
   end
   
end