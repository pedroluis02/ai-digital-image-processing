%  Funcion: recibe matriz de la imagen en escala de grises y retorna histograma
% imagen: matriz de la imagen
% cuadrante = [fila_inicio, fila_fin, columna_inicio, columna_fin]
	% fila_inicio y fila_fin: inicio y fin de fila del cuadrante de la imagen
	% columna_inicio y columna_fin : inicio y fin del columna del cuadrante de la imagen
% --- retorna h: histograma
function h = histogramaGray(imagen, cuadrante)  
   h = zeros(256, 1); % inicializa el histograma
   
   fila_inicio = cuadrante(1); fila_fin = cuadrante(2);
   columna_inicio = cuadrante(3); columna_fin = cuadrante(4);
   
   for i = fila_inicio : fila_fin
        for j = columna_inicio : columna_fin
             k = imagen(i, j);
             h(k + 1) = h(k + 1) + 1;
        end
    end
end