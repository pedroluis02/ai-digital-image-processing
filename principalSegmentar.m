function principalSegmentar(imagen)
   if is_sq_string(imagen)
      imagen_entrada = imread(imagen);
   elseif is_real_scalar(imagen)
      error('No es una matriz de imagen valida.');
      imagen_entrada = [];
      return;
   else
      imagen_entrada = imagen;
   end
   
   imagen_gray = escalaGrises(imagen_entrada, 1);
   matrizBordes = obtenerMatrizBorde(imagen_gray, 14, 14);
   imagen_seg = segmentarImagen(imagen_gray, matrizBordes);
   figure(1);
   subplot(1, 2, 1); 
   imshow(imagen_gray); title('Original');
   subplot(1, 2, 2);  
   imshow(imagen_seg); title('Segmentada');
end