## Digital Image Processing
Sample UI for Digital image processing using AI (Artificial intelligence).

###### Features
* Gray scale.
* Binarization: global, local and threshold.
* Histogram: RGB and gray scale.
* Filters: Average and Gauss.
* Compare: Quadrants(Buckets) and normalized histogram.
* Segment area.
* Dimension of segmented area.
* Compare two images, is it moving?: left, top, right or bottom.
