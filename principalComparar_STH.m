% imagen_1 y imagen_2 deben ser la misma imagen pero
% deberan poseer diferente dimension
function principalComparar_STH(imagen_1, imagen_2)
   imagen_1 = imread(imagen_1);
   imagen_2 = imread(imagen_2);
   
   gray_1 = escalaGrises(imagen_1, 1);
   gray_2 = escalaGrises(imagen_2, 1); 
   
   [f1, c1] = size(gray_1);
   [f2, c2] = size(gray_2);
   
   h1 = histogramaGray(gray_1, [1 f1 1 c1]);
   h2 = histogramaGray(gray_2, [1 f2 1 c2]);
   
   LON = 256;
   
   figure(4);
   subplot(2, 2, 1); imshow(gray_1); title('Imagen 1');
   subplot(2, 2, 3); imshow(gray_2); title('Imagen 2');  
   
   % Tamaño de images
   t1_est = f1 * c1;
   t2_est = f2 * c2;
   
   % El menor tamaño de usara para estandarizar los histogramas
   T_Est = 0; 
   if t1_est > t2_est
       T_Est = t2_est;
   else
       T_Est = t1_est;
   end   
   
   valor1_m1 = T_Est / t1_est; % Valor de multiplicación para imagen_1
   valor2_m2 = T_Est / t2_est; % Valor de multiplicación para imagen_2
   
   h1_E = round(h1 * valor1_m1);
   h2_E = round(h2 * valor2_m2);
   
   subplot(2, 2, 2);
     bar(1 : 1 : LON, h1_E, 'facecolor', 'r', 'edgecolor', 'r'); 
     title('Histograma estandarizado de imagen 1');
     
     subplot(2, 2, 4);
     bar(1 : 1 : LON, h2_E, 'facecolor', 'b', 'edgecolor', 'b'); 
     title('Histograma  estandarizado de imagen 2'); 
   % ----------------------------------------------------------
   
   v1 = 0; v2 = 0; resta = 0; 
   SILIMITUD = 40; ERROR = 15; 
   
   descriptor = zeros(LON, 1);
   for i=1: LON
      v1 = h1_E(i);
      v2 = h2_E(i);
      
      resta = abs(v1 - v2);
      %disp(resta);
      if resta < SILIMITUD
         descriptor(i) = 1; % Igual
      end
   end
   
   total_s = sum(descriptor);
   printf('Suma Total: %d\n', total_s);
   
   if total_s >= (LON - ERROR)
       disp('Imágenes iguales');
   else
       disp('Imágenes diferentes');
   end
   
end