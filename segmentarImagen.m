% Funcion: recibe una imagen en escala de grises y una matriz de bordes
  % matrizBordes: matriz de bordes encontrados
% nueva_Imagen: imagen segmentada
function nueva_Imagen = segmentarImagen(imagen, matrizBordes)
   [filas, columnas] = size(imagen);
   nv = zeros(filas, columnas) + 255;
   
   for i = 1: filas
      if matrizBordes(i, 1) ~= -1 && matrizBordes(i, 2) ~= -1
      	b = matrizBordes(i, 1);
      	while  b <= matrizBordes(i, 2)
      	   nv(i, b) = 0;
      	   b = b + 1;
      	end
      end 
   end
   
   nueva_Imagen = nv;
   
end