% imagen_1: imagen 1
% imagen_2: imagen 2
% particiones: partciones de ancho y alto
% reduccion: numero de valores a reducir del histograma de 256 por cuadante
function r = compararCuadrantesImagenes(imagen_1, imagen_2, particiones, reduccion)
   cuadrante_1 = cuadrantesImagen(imagen_1, particiones);	
   cuadrante_2 = cuadrantesImagen(imagen_2, particiones);
   i = 1; j = 1; des = 0;
   [fila, columna] = size(cuadrante_1);
   c1 = []; c2 = [];
   h1 = []; h2 = [];
   h1_reduc = []; h2_reduc = [];
   difer = zeros(fila, 1);
   while i <= fila
      c1 = cuadrante_1(i, :);
      h1 = histogramaGray(imagen_1, c1);
      h1_reduc = reducirHistograma16(h1, reduccion);
      
      c2 = cuadrante_2(i, :);
      h2 = histogramaGray(imagen_2, c2);
      h2_reduc = reducirHistograma16(h2, reduccion);
      
      j = 1; des = 0;
      while j <= reduccion
      	res = abs(h1_reduc(j) - h2_reduc(j));
      	if res < 4 % Nivel de similitud por cuadrante
      	   des = des  + 1;
      	end
      	j = j + 1;
      end
  
      if des >= (reduccion - 2) % Nivel de error
         difer(i) = 1;
      end
      
      i = i + 1;
      
   end
   
   r = (sum(difer) == fila); % comparación final
   
end