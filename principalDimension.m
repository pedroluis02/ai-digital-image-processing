function principalDimension(imagen, ancho_real, alto_real)
   img = imread(imagen);   
   imagen_gray = escalaGrises(img, 1);
   matrizBordes = obtenerMatrizBorde(imagen_gray, 14, 14);
   imagen_seg = segmentarImagen(imagen_gray, matrizBordes);
   
   figure(3);
   subplot(1, 2, 1); 
   imshow(imagen_gray); title('Original');
   subplot(1, 2, 2);  
   imshow(imagen_seg); title('Detecion de borde');
   
   [filas, columas] = size(matrizBordes);
   
   i = 1; j = filas;
   len = round(filas / 2);
   distancia_w = 0; fila_1 = 0; fila_2 = 0;
   while i <=  len
      
      if matrizBordes(i, 1) != -1 && matrizBordes(i, 2) != -1
      	if fila_1 == 0
      	   fila_1 = i;
      	end
      	d = abs(matrizBordes(i, 1) - matrizBordes(i, 2) );
      	if d > distancia_w
      	   distancia_w = d;
      	end
      end
      
      if matrizBordes(j, 1) != -1 && matrizBordes(j, 2) != -1
      	if fila_2 == 0
      	   fila_2 = j;
      	end
      	d = abs(matrizBordes(j, 1) - matrizBordes(j, 2) );
      	if d > distancia_w
      	   distancia_w = d;
      	end
      end
      
      i = i + 1;
      j = j - 1;
      
   end
   
   f = abs(fila_1 - fila_2);
   [alto, ancho] = size(imagen_gray);
   % alto de porcion
   an_real = (distancia_w * ancho_real) / ancho;
   % alto de porcion
   al_real = (f * alto_real) / alto;
   
   disp([ 'La Dimension es (ancho, alto): ' num2str(an_real) ' ' num2str(al_real)])
   
end