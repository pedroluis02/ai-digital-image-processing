% imagen: imagen en escala de grises
% particiones: numero de cuadrantes. Ejemplo: 3 part. genera 9 cuadrantes
% cuadrantes_formados: [fila_inicio, fila_fin, columna_inicio, columna_fin]
function cuadrantes_formados = cuadrantesImagen(imagen, particiones)
   [filas, columnas] = size(imagen);
   
   fila_cuadrante = round(filas / particiones);
   columna_cuadrante = round(columnas / particiones);
   cuadrantes_formados = zeros(particiones * particiones, 4);
   % ---------
   i = 1; j = 1; 
   fila_inicio = 0; fila_fin = 0;
   columna_inicio = 0; columna_fin = 0; 
   k = 1;
   %----------
   while i <= particiones
      fila_inicio = (i - 1) * fila_cuadrante + i; 
      if i == (particiones)
         fila_fin = filas;
      else 
         fila_fin = fila_inicio + fila_cuadrante;
      end
      
      while j <= particiones
         
         columna_inicio = (j - 1) * columna_cuadrante + j;
         
         if j == (particiones)
            columna_fin = columnas;
         else 
            columna_fin = columna_inicio + columna_cuadrante;
         end
         
         cuadrantes_formados(k, 1) = fila_inicio; 
         cuadrantes_formados(k, 2) = fila_fin; 
         
         cuadrantes_formados(k, 3) = columna_inicio; 
         cuadrantes_formados(k, 4) = columna_fin; 
         
         j = j + 1;
         k = k + 1;
         
      end
      
      j = 1;
      i = i + 1;
      
   end
   
end