% Images de entrada en rgb
% Las imagenes deben ser de igual dimension
function principalComparar(imagen_1, imagen_2)
   img_1 = imread(imagen_1);
   img_2 = imread(imagen_2);
   gray_1 = escalaGrises(img_1, 1);
   gray_2 = escalaGrises(img_2, 1); 
   
   figure(2);
   subplot(1, 2, 1); 
   imshow(gray_1); title('Imagen 1');
   subplot(1, 2, 2);  
   imshow(gray_2); title('Imagen 2');
   
   r = compararCuadrantesImagenes(gray_1,  gray_2, 3, 16);
   if r == 1
      disp('Imágenes iguales');
   else
      disp('Imágenes diferentes');
   end
end 