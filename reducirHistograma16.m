% reduce el histograma de 256 a 16 valores
function h16 = reducirHistograma16(histograma256, reduction)
   i = 1; promedio = 0;
   h16 = zeros(reduction, 1);
   v1 = 1; v2 = reduction;
   while i <= reduction
      h16(i) = round( sum( histograma256(v1 : v2) ) / reduction );
      
      v1 = v1 + reduction;
      v2 = v2 + reduction;
      
      i  = i + 1;
   end
end